#include "struc.h"

struc::struc()
{
    scale = 1.0/1.0; //inputdata is mm
    nr = 0.1;
    //ctor
}

struc::~struc()
{
    //dtor
}

template <typename T> void struc::t_out(const char* name, T out){
    cout << name << " = " << out << endl;
}

void struc::setStructure(){
    cout << "set structure" << endl;
    openfile();
}

void struc::split(string ss, string out[] , int id){
    char cc[80];
    strcpy(cc,ss.c_str());
    char s2[] = " ,";
    char* tok;
    tok = strtok( cc, s2 );
//    t_out("tok_1",tok);
//    cout <<" void split size of out = " << sizeof(out)/sizeof(string)<<"\n";
    int i = 0;
    while( tok != NULL ){

        if (i >= id){
//            t_out("ERROR i > id",i);
            exit(1);
        }
//        //t_out( "tok", tok );
        out[i]=tok;
        ++i;
        tok = strtok( NULL, s2 );  // 2âÒñ⁄à»ç~

    }

}

void struc::file_open_check(bool file,const char* fname){
    if(!file){
        char foe[] = "file open Error file is ";
        t_out(foe,fname);
        exit(1);
    }
}


void struc::openfile(){
    fcood.open("/Users/YujiTAKIUCHI/Documents/of_v0.7.4_osx_release/apps/myApps/TRUSS/bin/data/COOD.txt",ios_base::in);
    ficon.open("/Users/YujiTAKIUCHI/Documents/of_v0.7.4_osx_release/apps/myApps/TRUSS/bin/data/ICON.txt",ios_base::in);
    fout.open("/Users/YujiTAKIUCHI/Documents/of_v0.7.4_osx_release/apps/myApps/TRUSS/bin/OUTPUTDATA.txt",ios_base::out | ios_base::trunc);

//    cout << fout << endl;
    file_open_check(fout,"data/OUT");
    file_open_check(fcood,"data/COOD");
    file_open_check(ficon,"data/ICON");

    string ss;
    const int outmaxid = 20;
    while(!fcood.eof()){
        getline(fcood,ss);

        string out1[outmaxid];
        split(ss,out1,outmaxid);

        set_cood(out1,outmaxid);
    }
    fcood.close();
    numnodes = cood.size();
    t_out("numnodes" , numnodes);
    
    while (!ficon.eof()) {
        getline(ficon, ss);
        string out2[outmaxid];
        split(ss,out2,outmaxid);
        
        set_icon(out2, outmaxid);
    }
    ficon.close();
    members = icon.size();
    t_out("members",members);
    
//    set_node_to_icon();
}


void struc::set_cood(string out[], int index){
    const int localindex = 4;
    istringstream is[localindex];
    double xyz[localindex];
    int ii;
    for(int i = 0; i<localindex; i++){
        is[i].str(out[i]);
    }

    is[0] >> ii;
    double jj;
    for(int i=1; i<4; ++i){
//        is[i] >> xyz[i];
        is[i] >> jj;
//        t_out("jj",jj);
        xyz[i] = jj;
//        t_out("i",i);
//        t_out("xyz[i]",xyz[i]);
    }
//    t_out("ii",ii);
//    node n;
//    n.set(ii,xyz[1],xyz[2],xyz[3],scale);
    node2d n;
    n.set(ii, xyz[1], xyz[2], scale);
//    n.out();
    cood.push_back(n);
//    mesh.addcolor(ofColor(119,199,210));
//    mesh.addVertex(n.Pd);


}


void struc::set_icon(string out[], int index){
    const int localindex = 4;
    istringstream is[localindex];
    double nn[localindex];
    
    for(int i = 0; i<localindex; i++){
        is[i].str(out[i]);
    }
    
    double jj;
    for(int i=0; i<4; ++i){
        is[i] >> jj;
        nn[i] = jj;
    }
    
    memb m;
    m.set_geo(nn[0],nn[1],nn[2],nn[3],scale);
    //    n.out();
    icon.push_back(m);
    //    mesh.addcolor(ofColor(119,199,210));
    //    mesh.addVertex(n.Pd);
    //    mesh.addColor(ofFloatColor(119.0/255.0,199.0/255.0,210.0/255.0));
}

void struc::customDraw(){
//nr is radius of node;
//    cout << "numnodes" << numnodes << endl;
/*
    for(int i=0; i<numnodes; i++){
//        cood[i].set_r(nr);
//        cood[i].draw();
    }*/
    
    glPointSize(5.0);
//    mesh.drawVertices();
    ofEnableBlendMode(OF_BLENDMODE_ADD);
    vbo_points.draw(GL_POINTS,0,numnodes);
    glLineWidth(1.0);
    vbo_lines.draw(GL_LINES, 0, members * 2);

}
void struc::genMesh(){
    int *test1 = new int[10];
    ofVec3f *myVerts = new ofVec3f[numnodes]; // í∏ì_ÇÃç¿ïW
    ofFloatColor *myColor = new ofFloatColor[numnodes]; // í∏ì_ÇÃêFèÓïÒ
    ofVec3f *myLines = new ofVec3f[members * 2];
    ofFloatColor *myLineColor = new ofFloatColor[members * 2];
    
    for(int i=0; i<numnodes; i++){
        myVerts[i].set(cood[i].Pd.x,cood[i].Pd.y,cood[i].Pd.z);
        myColor[i].set(119.0/255.0,199.0/255.0,210.0/255.0,1.0);
    }
    int ns,ne;
    int nn = 0;
    for(int i=0; i<members; i++){
        ns = icon[i].n_S;
        ne = icon[i].n_E;
        myLines[nn].set(cood[ns].Pd.x,cood[ns].Pd.y,cood[ns].Pd.z);
        myLineColor[nn].set(119.0/255.0,199.0/255.0,210.0/255.0);
        nn++;
        myLines[nn].set(cood[ne].Pd.x,cood[ne].Pd.y,cood[ne].Pd.z);
        myLineColor[nn].set(119.0/255.0,199.0/255.0,210.0/255.0);
        nn++;
    }
    
    vbo_points.setVertexData(myVerts,numnodes,GL_DYNAMIC_DRAW);
    vbo_points.setColorData(myColor,numnodes,GL_DYNAMIC_DRAW);
    vbo_lines.setVertexData(myLines, members*2, GL_DYNAMIC_DRAW);
    vbo_lines.setColorData(myLineColor, members*2,GL_DYNAMIC_DRAW);
}


int struc::get_members(){
    return members;
}

int struc::get_nodes(){
    return numnodes;
}

ofVec3f struc::get_cood(int index){
    ofVec3f DisplayedPos = cood[index].Pd;
    
    return DisplayedPos;
}

ofVec3f struc::get_ccood(int index){
    ofVec3f DisplayedPos = icon[index].get_cpd();
    return DisplayedPos;
}


void struc::set_node_to_icon(){
    for (int i = 0; i<members; i++) {
        int NS = icon[i].n_S;
        int NE = icon[i].n_E;
        
        icon[i].set_node(cood[NS].P,cood[NE].P);
    }
}


