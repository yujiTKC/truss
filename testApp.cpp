#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
    gui = new ofxUICanvas();
    
    r = 100; g = 100; b = 100;
    gui->addLabel("OFXUI TUTORIAL", OFX_UI_FONT_LARGE);
    gui->addSpacer();
    gui->addSlider("RED",0.0,255.0,r);
    gui->addSlider("GREEN",0.0,255.0,g);
    gui->addSlider("BLUE",0.0,255.0,b);
    gui->addToggle("FULLSCREEN", false);
    gui->autoSizeToFitWidgets();
    ofAddListener(gui->newGUIEvent, this, &testApp::guiEvent);
    gui->loadSettings("GUI/guiSettings.xml");
    
    ofBackground(r,g,b);
    
    st.setStructure();
    
}

//--------------------------------------------------------------
void testApp::update(){

}

//--------------------------------------------------------------
void testApp::draw(){
    st.draw();
}

//--------------------------------------------------------------
void testApp::exit(){
    gui->saveSettings("GUI/guiSettings.xml");
    delete gui;
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::guiEvent(ofxUIEventArgs &e)
{
    if(e.widget->getName() == "RED")
    {
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        r = slider->getScaledValue();
    }
    else if(e.widget->getName() == "GREEN")
    {
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        g = slider->getScaledValue();
    }
    else if(e.widget->getName() == "BLUE")
    {
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        b = slider->getScaledValue();
    }
    else if(e.widget->getName() == "FULLSCREEN")
    {
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        ofSetFullscreen(toggle->getValue());
    }
    ofBackground(r,g,b);
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}