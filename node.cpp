#include "node.h"
using namespace std;
node::node(){
    //ctor
}

node::~node(){
    //dtor
}

void node::set(int i,double xx,double yy,double zz,float scale){
    index = i;
    P.set(xx,yy,zz);
    Pd.set(xx*scale,yy*scale,zz*scale);
//    cout << "xx = " << xx << "p.x =" << P.x << endl;
    /*
    x = xx; P.x = x*scale;
    y = yy; P.y = y*scale;
    z = zz; P.z = z*scale;
*/
}

void node::out(){
    cout << "NODE " << index << " x = " << P.x << " y = " << P.y << " z = " << P.z << endl;
}

void node::set_r(float nr){
    r = nr;
}

void node::customDraw(){
//    ofPoint(P);
    ofSetColor(Ncolor);
    ofSphere(Pd,r);
}


void node2d::set(int i,double xx,double yy,float scale){
    index = i;
    P.set(xx,yy);
    Pd.set(xx*scale,yy*scale);
}

void node2d::out(){
    cout << "NODE " << index << " x = " << P.x << " y = " << P.y << endl;
}

void node2d::customDraw(){
    ofSetColor(Ncolor);
    ofCircle(P.x, P.y, r);
}
