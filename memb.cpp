//
//  memb.cpp
//  takidas_mv01_1
//
//  Created by 滝内 雄二 on 2013/06/17.
//
//

#include "memb.h"

memb::memb(){
    
}

memb::~memb(){
    //dtor
}

void memb::set_geo(int ii,int n_start, int n_end, int n_mate, float sca){
    index = ii;
    n_S = n_start-1;
    n_E = n_end-1;
    n_M = n_mate-1;
    
    scale = sca;
    //nSnEnMは配列の場所を示すように使いたいので−１　(入力データは１〜配列は０〜)
}

void memb::set_node(ofVec3f ns , ofVec3f ne){
    P_s = ns;
    P_e = ne;
    
    CP = ofVec3f((P_s.x+P_e.x)*0.5,(P_s.y+P_e.y)*0.5,(P_s.z+P_e.z)*0.5);
    CPd = ofVec3f(CP.x*scale,CP.y*scale,CP.z*scale);
}

void memb::out(){
    cout << "nS =" << n_S << " nE = " << n_E << " nM = " << n_M << endl;
}

ofVec3f memb::get_cpd(){
    return CPd;
}

