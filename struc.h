#ifndef STRUC_H
#define STRUC_H

#include "ofMain.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring> // for strcpy
#include <cstdlib>
#include <sstream>
#include <vector>
#include "node.h"
#include "memb.h"
//#include "eigen_3_1_3/Eigen/Core"


using namespace std;


class struc : public ofNode
{
    public:
        struc();
        virtual ~struc();

        template <typename T>
		void t_out(const char* name, T out);
    
        void setStructure();
    
        void customDraw();
        void genMesh();
        int get_nodes();
        int get_members();
        ofVec3f get_cood(int index);
        ofVec3f get_ccood(int index);

    protected:
    private:
        ifstream fcood,ficon;
        ofstream fout;
        void file_open_check(bool file,const char* fname);
        void set_cood(string out[], int index);
        void set_icon(string out[], int index);
        void openfile();
//    vector < node > cood;
        vector < node2d > cood;
        vector< memb > icon;
        int numnodes,members;
        void set_node_to_icon();
        void split(string ss, string out[] , int id);
        float scale,nr;
        ofMesh mesh;
        ofVbo vbo_points,vbo_lines;

};

#endif // STRUC_H
