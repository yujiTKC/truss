//
//  memb.h
//  takidas_mv01_1
//
//  Created by 滝内 雄二 on 2013/06/17.
//
//

#ifndef __takidas_mv01_1__memb__
#define __takidas_mv01_1__memb__

#include "ofMain.h"
#include <iostream>
#include <iomanip>

class memb : public ofNode
{
public:
    memb();
    virtual ~memb();
    void set_geo(int ii,int n_start,int n_end,int n_mate, float sca);
    void set_node(ofVec3f ns, ofVec3f ne);
    void out();
    int n_S,n_E,n_M;
    ofVec3f get_cpd();
protected:
private:
    ofVec3f P_s,P_e,CP,CPd;
    int index;
    float r,scale;
    
    
};

#endif /* defined(__takidas_mv01_1__memb__) */
