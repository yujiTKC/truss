#ifndef NODE_H
#define NODE_H

#include "ofMain.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>

class node : public ofNode
{
    public:
        node();
        virtual ~node();
        void set(int i,double xx,double yy,double zz,float scale);
        void out();
        void set_r(float nr);
        void customDraw();
        ofVec3f P,Pd;
        ofColor Ncolor;
    protected:
    private:

        int index;
        float r;


};

class node2d : public node
{
public:
    void set(int i,double xx,double yy,float scale);
    void out();
    void customDraw();
    ofVec2f P,Pd;
private:
    int index;
    float r;

};

#endif // NODE_H
